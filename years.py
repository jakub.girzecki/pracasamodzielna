#3. Napisz funkcję leap_years, która przyjmuje parametr n i wyświetli n najbliższych lat przestępnych. Zdefiniuj
#w tym celu funkcję is_leap, która zwróci wartość True gdy rok jest przestępny (podzielny przez 4 i niepodzielny przez 100,
#lub podzielny przez 400: https://pl.wikipedia.org/wiki/Rok_przest%C4%99pny), a następnie wykorzystaj pętlę while.


def leap_years():
    n = int(input("podaj rok:"))
    while (n):
        if n % 4 == 0 and (n % 100 != 0 or n % 400 == 0):
            print(n, n+4, n+8, n+12, end="")
            break
        else:
            n += 1
if __name__ == '__main__':
    leap_years()
    print("Może się uda")

